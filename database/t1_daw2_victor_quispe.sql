drop database if exists t1_daw2_victor_quispe; 
create database t1_daw2_victor_quispe;
use t1_daw2_victor_quispe;

create table Publicacion (
	id_publicacion int auto_increment primary key,
    nombre_publicacion varchar(100) not null,
    contenido_publicacion varchar(255) not null,
    fecha_publicacion datetime not null
);
	
create table Etiqueta (
	id_etiqueta int auto_increment primary key,
    nombre_etiqueta varchar(100) not null,
    fecha_etiqueta datetime not null,
    etiqueta_publicacion int,
    constraint fk_etiqueta_publicacion foreign key (etiqueta_publicacion) references Publicacion (id_publicacion)
);

create table Comentario (
	id_comentario int auto_increment primary key,
    contenido_comentario varchar(255) not null,
    fecha_comentario datetime not null,
    comentario_publicacion int,
    constraint fk_comentario_publicacion foreign key (comentario_publicacion) references Publicacion (id_publicacion)
);

-- select e.* from Etiqueta e where e.etiqueta_publicacion = 1;
-- select c.* from Comentario c where c.comentario_publicacion = 1;

-- Prueba de Inserts

insert into Publicacion values (null, "Gallinitas al ataque", "Nueva peli de fin de semana para ver en familia", now());
insert into Etiqueta values (null, "Cine", now(), 1);
insert into Comentario values (null, "Muy buena pelicula", now(), 1);

insert into Etiqueta values (null, "Pelicula", now(), 1);
insert into Comentario values (null, "Esta pelicula es de mi primito", now(), 1);

insert into Publicacion values (null, "Milagros de Juan", "Esta es tragicomedia buenarda", now());
insert into Etiqueta values (null, "General", now(), 2);
insert into Comentario values (null, "Mi humilde comentario por parte de Juan", now(), 2);

insert into Publicacion values (null, "Los gatitos tierno", "Que bonitos son los gatitos de noche", now());
insert into Etiqueta values (null, "Gatitos", now(), 3);
insert into Comentario values (null, "Deberia llamarse ataque a mi calamina", now(), 3);

/*
*
*

select * from Publicacion;
select * from Etiqueta;
select *from Comentario;

*
*
*/