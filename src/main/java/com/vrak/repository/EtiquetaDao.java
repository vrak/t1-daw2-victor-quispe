package com.vrak.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vrak.model.Etiqueta;

@Repository
public interface EtiquetaDao extends CrudRepository<Etiqueta, Integer>{

	public List<Etiqueta> findAll();
}
