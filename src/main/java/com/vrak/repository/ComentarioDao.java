package com.vrak.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.vrak.model.Comentario;

@Repository
public interface ComentarioDao extends CrudRepository<Comentario, Integer>{

	public List<Comentario> findAll();
	
	/*@Query("select cm from Comentario cm where cm.comentario_publicacion = ?1")
	public List<Comentario> findComentariosByIdPublicacion(@Param("comentario_publicacion") Publicacion idPublicacion);*/

}
