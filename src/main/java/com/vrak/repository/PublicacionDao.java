package com.vrak.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.vrak.model.Publicacion;

@Repository
public interface PublicacionDao extends CrudRepository<Publicacion, Integer> {

	public List<Publicacion> findAll();
	
	// Buscar publicacion por ID
	@Query("select p from Publicacion p where p.id_publicacion = ?1")
	public Publicacion buscarPublicacionByID(@Param("id_publicacion") int id);
}
