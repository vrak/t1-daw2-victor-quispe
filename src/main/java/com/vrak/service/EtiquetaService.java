package com.vrak.service;

import java.util.List;

import com.vrak.model.Etiqueta;

public interface EtiquetaService {

	public List<Etiqueta> listarEtiquetas();
	public Etiqueta insertarEtiqueta(Etiqueta etiqueta);
}
