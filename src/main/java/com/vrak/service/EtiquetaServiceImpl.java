package com.vrak.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vrak.model.Etiqueta;
import com.vrak.repository.EtiquetaDao;

@Service
public class EtiquetaServiceImpl implements EtiquetaService {

	@Autowired
	private EtiquetaDao etiquetaRepo;
	
	@Override
	public List<Etiqueta> listarEtiquetas() {
		// TODO Auto-generated method stub
		return etiquetaRepo.findAll();
	}

	@Override
	public Etiqueta insertarEtiqueta(Etiqueta etiqueta) {
		// TODO Auto-generated method stub
		return etiquetaRepo.save(etiqueta);
	}

}
