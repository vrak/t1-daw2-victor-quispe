package com.vrak.service;

import java.util.List;

import com.vrak.model.Publicacion;

public interface PublicacionService {
	
	public List<Publicacion> listarPublicaciones();
	
	public Publicacion insertarPublicacion(Publicacion publicacion);
	
	public Publicacion buscarPublicacacionPorID(int id);
}
