package com.vrak.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vrak.model.Comentario;
import com.vrak.repository.ComentarioDao;

@Service
public class ComentarioServiceImpl implements ComentarioService {
	
	@Autowired
	private ComentarioDao comentarioRepo;

	@Override
	public List<Comentario> listarComentarios() {
		// TODO Auto-generated method stub
		return comentarioRepo.findAll();
	}

	@Override
	public Comentario insertarComentario(Comentario comentario) {
		// TODO Auto-generated method stub
		return comentarioRepo.save(comentario);
	}

	/*@Override
	public List<Comentario> listarComentariosPorIdPublicacion(Publicacion idPublicacion) {
		// TODO Auto-generated method stub
		return comentarioRepo.findComentariosByIdPublicacion(idPublicacion);
	}*/

}
