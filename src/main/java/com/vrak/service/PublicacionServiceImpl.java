package com.vrak.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vrak.model.Publicacion;
import com.vrak.repository.PublicacionDao;

@Service
public class PublicacionServiceImpl implements PublicacionService {

	@Autowired
	private PublicacionDao publicacionRepo;
	
	@Override
	public List<Publicacion> listarPublicaciones() {
		// TODO Auto-generated method stub
		return publicacionRepo.findAll();
	}

	@Override
	public Publicacion insertarPublicacion(Publicacion publicacion) {
		// TODO Auto-generated method stub
		return publicacionRepo.save(publicacion);
	}

	@Override
	public Publicacion buscarPublicacacionPorID(int id) {
		// TODO Auto-generated method stub
		return publicacionRepo.buscarPublicacionByID(id);
	}
	
	
}
