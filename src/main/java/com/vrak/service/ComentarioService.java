package com.vrak.service;

import java.util.List;

import com.vrak.model.Comentario;

public interface ComentarioService {
	
	public List<Comentario> listarComentarios();
	
	public Comentario insertarComentario(Comentario comentario);
	
	/*public List<Comentario> listarComentariosPorIdPublicacion(Publicacion idPublicacion);*/
}
