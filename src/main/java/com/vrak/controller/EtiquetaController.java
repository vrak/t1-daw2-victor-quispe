package com.vrak.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.vrak.model.Etiqueta;
import com.vrak.service.EtiquetaServiceImpl;

@Controller
public class EtiquetaController {

	@Autowired
	private EtiquetaServiceImpl etiquetaService;
	
	public List<Etiqueta> getEtiquetas(){
		return etiquetaService.listarEtiquetas();
	}
	
	public Etiqueta guardarEtiqueta(Etiqueta etiqueta) {
		return etiquetaService.insertarEtiqueta(etiqueta);
	}
}
