package com.vrak.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.vrak.model.Publicacion;
import com.vrak.service.PublicacionServiceImpl;

@Controller
public class PublicacionController {
	
	@Autowired
	private PublicacionServiceImpl publicacionService;
	
	public List<Publicacion> getPublicaciones(){
		return publicacionService.listarPublicaciones();
	}
	
	public Publicacion guardarPublicacion(Publicacion publicacion) {
		return publicacionService.insertarPublicacion(publicacion);
	}
	
	public Publicacion buscarPublicacionPorID(int id) {
		return publicacionService.buscarPublicacacionPorID(id);
	}
}
