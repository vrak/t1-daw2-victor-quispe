package com.vrak.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.vrak.model.Comentario;
import com.vrak.service.ComentarioServiceImpl;

@Controller
public class ComentarioController {

	@Autowired
	private ComentarioServiceImpl comentarioService;
	
	public List<Comentario> getComentarios() {
		return comentarioService.listarComentarios();
	}
	
	public Comentario guardarComentario(Comentario comentario) {
		return comentarioService.insertarComentario(comentario);
	}
	
	/*public List<Comentario> getComentariosByIdPublicacion(Publicacion idPublicacion) {
		return comentarioService.listarComentariosPorIdPublicacion(idPublicacion);
	}*/
}
