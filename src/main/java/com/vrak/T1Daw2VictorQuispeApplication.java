package com.vrak;

import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.vrak.controller.ComentarioController;
import com.vrak.controller.EtiquetaController;
import com.vrak.controller.PublicacionController;
import com.vrak.model.Comentario;
import com.vrak.model.Etiqueta;
import com.vrak.model.Publicacion;

@SpringBootApplication
public class T1Daw2VictorQuispeApplication {
	
	private static final String YELLOW_TEXT = "\u001B[38;2;255;226;93m";
	public static final String RESET = "\u001B[0m";
	public static final String RED = "\u001B[38;2;245;46;46m";
	public static final String GREEN = "\u001B[38;2;43;185;39m";

	public static void main(String[] args) {
		SpringApplication.run(T1Daw2VictorQuispeApplication.class, args);
	}
	
	@Bean
	CommandLineRunner mainExamen(PublicacionController controladorPublicacion, EtiquetaController controladorEtiqueta, ComentarioController controladorComentario) {
		return args -> {
			
			//SimpleDateFormat dtf = new SimpleDateFormat("yyyy/MM/dd");
	        Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			
			System.out.println("\n"
					+ "██╗███╗   ██╗███████╗███████╗██████╗ ████████╗███████╗\n"
					+ "██║████╗  ██║██╔════╝██╔════╝██╔══██╗╚══██╔══╝██╔════╝\n"
					+ "██║██╔██╗ ██║███████╗█████╗  ██████╔╝   ██║   ███████╗\n"
					+ "██║██║╚██╗██║╚════██║██╔══╝  ██╔══██╗   ██║   ╚════██║\n"
					+ "██║██║ ╚████║███████║███████╗██║  ██║   ██║   ███████║\n"
					+ "╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═╝  ╚═╝   ╚═╝   ╚══════╝\n"
					+ "\n" + RED + "* Los insert se hacen de manera automatica mediante Main" + RESET + "\n");
			// PUBLICACION 1
			Publicacion publicacion1 = new Publicacion();
			publicacion1.setNombre_publicacion("Hello My Friend?");
			publicacion1.setContenido_publicacion("Este es un saludito en ingles");
			publicacion1.setFecha_publicacion(date);
			
			// PUBLICACION 2
			Publicacion publicacion2 = new Publicacion();
			publicacion2.setNombre_publicacion("Mike Tyson gana pelea");
			publicacion2.setContenido_publicacion("Este jueves el boxeador fue vencedor");
			publicacion2.setFecha_publicacion(date);
			
			// GUARDANDO NUEVAS PUBLICACIONES
			controladorPublicacion.guardarPublicacion(publicacion1);
			System.out.println("Subida de publicación " + YELLOW_TEXT + publicacion1.getNombre_publicacion() + RESET +" exitosa!!");
						
			controladorPublicacion.guardarPublicacion(publicacion2);
			System.out.println("Subida de publicación " + YELLOW_TEXT + publicacion2.getNombre_publicacion() + RESET + " exitosa!!");
						
			
			if (publicacion1 != null && publicacion2 != null) {
				
				// ETIQUETA 1
				Etiqueta etiqueta1 = new Etiqueta();
				etiqueta1.setNombre_etiqueta("General");
				etiqueta1.setFecha_etiqueta(date);
				etiqueta1.setPublicacion(controladorPublicacion.getPublicaciones().get(3));
				
				// ETIQUETA 2
				Etiqueta etiqueta2 = new Etiqueta();
				etiqueta2.setNombre_etiqueta("Boxeo");
				etiqueta2.setFecha_etiqueta(date);
				etiqueta2.setPublicacion(controladorPublicacion.getPublicaciones().get(4));
				
				// COMENTARIO 1
				Comentario comentario1 = new Comentario();
				comentario1.setContenido_comentario("¿Que tal mi brosito como estas?");
				comentario1.setFecha_comentario(date);
				comentario1.setPublicacion(controladorPublicacion.getPublicaciones().get(3));
				
				// COMENTARIO 2
				Comentario comentario2 = new Comentario();
				comentario2.setContenido_comentario("Es un gran boxeador la verdad soy fan!!");
				comentario2.setFecha_comentario(date);
				comentario2.setPublicacion(controladorPublicacion.getPublicaciones().get(4));
				
				// GUARDANDO NUEVAS ETIQUETAS
				controladorEtiqueta.guardarEtiqueta(etiqueta1);
				System.out.println("\nRelacionando etiqueta " + YELLOW_TEXT + etiqueta1.getNombre_etiqueta() + RESET + " a la publicacion " + YELLOW_TEXT + etiqueta1.getPublicacion().getNombre_publicacion() + RESET);
					
				controladorEtiqueta.guardarEtiqueta(etiqueta2);
				System.out.println("Relacionando etiqueta " + YELLOW_TEXT + etiqueta2.getNombre_etiqueta() + RESET + " a la publicacion " + YELLOW_TEXT + etiqueta2.getPublicacion().getNombre_publicacion() + RESET);
					
				// GUARDANDO NUEVOS COMENTARIOS
				controladorComentario.guardarComentario(comentario1);
				System.out.println("\nRelacionando comentario " + YELLOW_TEXT + comentario1.getContenido_comentario() + RESET + " a la publicacion " + YELLOW_TEXT + comentario1.getPublicacion().getNombre_publicacion() + RESET);
					
				controladorComentario.guardarComentario(comentario1);
				System.out.println("Relacionando comentario " + YELLOW_TEXT + comentario2.getContenido_comentario() + RESET + " a la publicacion " + YELLOW_TEXT + comentario2.getPublicacion().getNombre_publicacion() + RESET);
			
				
			}
			
			
			System.out.println("\n"
					+ "███████╗███████╗ █████╗ ██████╗  ██████╗██╗  ██╗\n"
					+ "██╔════╝██╔════╝██╔══██╗██╔══██╗██╔════╝██║  ██║\n"
					+ "███████╗█████╗  ███████║██████╔╝██║     ███████║\n"
					+ "╚════██║██╔══╝  ██╔══██║██╔══██╗██║     ██╔══██║\n"
					+ "███████║███████╗██║  ██║██║  ██║╚██████╗██║  ██║\n"
					+ "╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝\n"
					+ "\n" + RED + "* Puede buscar el contenido de una publicidad por su ID" + RESET + "\n");
			Publicacion p = new Publicacion();
			p = controladorPublicacion.buscarPublicacionPorID(1);
			System.out.println("Búsqueda de publicacion hecho por Main");
			System.out.println(YELLOW_TEXT + "ID: " + RESET + p.getId_publicacion());
			System.out.println(YELLOW_TEXT + "Nombre" + RESET + p.getNombre_publicacion());
			System.out.println(YELLOW_TEXT + "Contenido: " + RESET + p.getContenido_publicacion());
			System.out.println(YELLOW_TEXT + "Fecha: " + RESET + p.getFecha_publicacion());
			
			int idPub;
			String rpta = null;
			boolean exit = false;
			do {
				Scanner sc = new Scanner(System.in);
				System.out.println(GREEN + "\n¿Desea buscar publicacion por ID? (Y/N)" + RESET);
				rpta = sc.next();
					
				switch(rpta) {
				case "y", "Y": 
					if(!rpta.isBlank() || !rpta.isEmpty() && rpta.equals("y")) {
						
						System.out.println(GREEN + "\nIngrese su ID: " + RESET);
						Scanner scId = new Scanner(System.in);
						idPub = scId.nextInt();
							try {
								Publicacion pub = new Publicacion();
								pub = controladorPublicacion.buscarPublicacionPorID(idPub);
								System.out.println("\nBúsqueda de publicacion hecho por Ti ♡");
								System.out.println(YELLOW_TEXT + "ID: " + RESET + pub.getId_publicacion());
								System.out.println(YELLOW_TEXT + "Nombre" + RESET + pub.getNombre_publicacion());
								System.out.println(YELLOW_TEXT + "Contenido: " + RESET + pub.getContenido_publicacion());
								System.out.println(YELLOW_TEXT + "Fecha: " + RESET + pub.getFecha_publicacion());
							} catch (Exception e) {
								System.out.println("\nEl Id no existe en nuestra fuente de datos");
							}
					} else {
							System.out.println("\nError al buscar");
						}
					break;
				case "n", "N":
					System.out.println("Gracias por usar el programa ♡");
					System.out.println("           |`-.._____..-'|\r\n"
							+ "           :  > .  ,  <  :\r\n"
							+ "           `./ __`' __ \\,'\r\n"
							+ "            | (|_) (|_) |\r\n"
							+ "            ; _  .  __  :\r\n"
							+ "            `.,' - `-. ,'\r\n"
							+ "              `, `_  .'\r\n"
							+ "              /       \\\r\n"
							+ "             /         :\r\n"
							+ "            :          |_\r\n"
							+ "           ,|  .    .  | \\\r\n"
							+ "          : :   \\   |  |  :\r\n"
							+ "          |  \\   :`-;  ;  |\r\n"
							+ "          :   :  | /  /   ;\r\n"
							+ "           :-.'  ;'  / _,'`------.\r\n"
							+ "           `'`''-`'''-'-''--.---  )\r\n"
							+ "                        BYE `----'");
					exit = true;
					break;
				default :
					System.out.println("Escriba Yes(y) o No(n) ");
				}
			} while(!exit);
			
			
			
			// ENVIANDO ID DE PUBLICACION PARA VER LOS COMENTARIOS ASOCIADOS A ESA PUBLICACION
			
			/*List<Comentario> commentsX = new ArrayList<Comentario>();
			commentsX = controladorComentario.getComentariosByIdPublicacion(controladorPublicacion.getPublicaciones().get(0));
			System.out.println("\nID: " + commentsX.get(0).getId_comentario());
			System.out.println("Comentario: " + commentsX.get(0).getContenido_comentario());
			System.out.println("Fecha: " + commentsX.get(0).getFecha_comentario() + "\n");
			
			for(int i = 0; i > commentsX.size(); i++) {
				System.out.println("\nID: " + commentsX.get(i).getId_comentario());
				System.out.println("Comentario: " + commentsX.get(i).getContenido_comentario());
				System.out.println("Fecha: " + commentsX.get(i).getFecha_comentario() + "\n");
			}*/
			
		};
	}

}
