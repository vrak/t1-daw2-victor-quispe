package com.vrak.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Publicacion")
public class Publicacion implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_publicacion")
	int id_publicacion;
	
	@Column(name = "nombre_publicacion")
	String nombre_publicacion;
	
	@Column(name = "contenido_publicacion")
	String contenido_publicacion;
	
	@Column(name = "fecha_publicacion")
	Date fecha_publicacion;
	
	@OneToMany(mappedBy = "publicacion")
	List<Etiqueta> etiquetas;
	
	@OneToMany(mappedBy = "publicacion")
	List<Comentario> comentarios;
}
