package com.vrak.model;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Etiqueta")
public class Etiqueta implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_etiqueta")
	int id_etiqueta;
	
	@Column(name = "nombre_etiqueta")
	String nombre_etiqueta;
	
	@Column(name = "fecha_etiqueta")
	Date fecha_etiqueta;
	
	@ManyToOne
	@JoinColumn(name = "etiqueta_publicacion")
	Publicacion publicacion;
	

}
